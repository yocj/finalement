let texte;
const parcours = document.createTreeWalker(document.body, NodeFilter.SHOW_TEXT, null, false);
const regex = new RegExp("au final");
const regex2 = new RegExp("Au final");

while (texte = parcours.nextNode()) {
	texte.nodeValue = texte.nodeValue.replace(regex, "finalement");
	texte.nodeValue = texte.nodeValue.replace(regex2, "Finalement");
}